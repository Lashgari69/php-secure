<?php
    require './functions.php';
//    error_reporting(0);
//    ini_set('display_errors' , 'off');

    setcookie('fullname' , 'tracket' , strtotime('+60 days') , '/' , null , null , true);

    $db = new PDO('mysql:host=127.0.0.1;dbname=exam','root','');
    if(isset($_POST['email'])) {
        $email = $_POST['email'];

        $user = $db->prepare("SELECT * FROM users WHERE email = :email");
        $user->execute([
            'email' => $email
        ]);

        if($user->rowCount()) {
            if(isset($_POST['password'])) {
                $password = $_POST['password'];
                $user = $user->fetchObject();
                if(password_verify($password , $user->password)) {
                    $_SESSION['user'] = $user;
                    echo 'password is correct';
                } else {
                    echo 'password isn`t correct';
                }
            }
        } else {
            echo 'the user isn`t exists';
        }
    }
?>


<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Password</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
</head>
<body>
    <div class="container">
        <div class="row">
            <div class="col-6 mt-5">
                <form method="post">
                    <div class="form-group">
                        <label for="email">Email address</label>
                        <input type="email" class="form-control" id="email" name="email" placeholder="Enter email">
                    </div>
                    <div class="form-group">
                        <label for="password">Password</label>
                        <input type="password" class="form-control" id="password" name="password" placeholder="Password">
                    </div>
                    <div class="form-group form-check">
                        <input type="checkbox" class="form-check-input" id="rememberme" name="rememberme">
                        <label class="form-check-label" for="rememberme">Remember Me</label>
                    </div>
                    <button type="submit" class="btn btn-primary">Login</button>
                </form>
            </div>
        </div>
    </div>
</body>
</html>

