<?php

    error_reporting(0);
    ini_set('display_errors' , 'off');

require './functions.php';

if($_POST['_csrf'] !== $_SESSION['_csrf']) {

    $db = new PDO('mysql:host=127.0.0.1;dbname=exam','root','');

    if(isset($_POST['email']) && isset($_POST['password'])) {
        
        $name = $_POST['name'];
        $email = $_POST['email'];
        $password = password_hash($_POST['password'],PASSWORD_DEFAULT,['cost' => 10]);
        $user = $db->prepare("insert into exam.users(name ,email, password) VALUE (:name ,:email,:password)");
        $user->bindParam(':name',$name);
        $user->bindParam(':email',$email);
        $user->bindParam(':password',$password);
        $user->execute();
    }
}else{
    echo "csrf token invalid";
}
?>


<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Insert</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
</head>
<body>
    <div class="container">
        <div class="row">
            <div class="col-6 mt-5">
                <form method="post">
                <input type="hidden" name="_csrf" value="<?php echo $_SESSION['_csrf'] ?>">
                    <div class="form-group">
                        <label for="name">Name</label>
                        <input type="text" class="form-control" id="name" name="name" placeholder="Enter name">
                    </div>
                    <div class="form-group">
                        <label for="email">Email address</label>
                        <input type="email" class="form-control" id="email" name="email" placeholder="Enter email">
                    </div>
                    <div class="form-group">
                        <label for="password">Password</label>
                        <input type="password" class="form-control" id="password" name="password" placeholder="Password">
                    </div>
                    <div class="form-group form-check">
                        <input type="checkbox" class="form-check-input" id="rememberme" name="rememberme">
                        <label class="form-check-label" for="rememberme">Remember Me</label>
                    </div>
                    <button type="submit" class="btn btn-primary">register</button>
                </form>
            </div>
        </div>
    </div>
</body>
</html>

