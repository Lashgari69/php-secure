<?php
    require './functions.php';

    $user = $_SESSION['user'];
    if(! $user) {
        die('user is not exists');
    }
?>


<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>User</title>
</head>
<body>
    <ul>
        <li>name : <?php echo $user->name;  ?> </li>
        <li>about me : <?php echo e($user->about) ?></li>
    </ul>



    <form action="/delete.php" method="POST">
        <input type="hidden" name="_csrf" value="<?php echo $_SESSION['_csrf'] ?>">
        <button type="submit">Delete Account</button>
    </form>
</body>
</html>



