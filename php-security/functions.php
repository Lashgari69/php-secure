<?php
session_start();

if($_SERVER['REQUEST_METHOD'] == 'POST') {
    if(! isset($_POST['_csrf']) || ($_POST['_csrf'] !== $_SESSION['_csrf']) ) {
        die('Invalid csrf token');
    }
}

$_SESSION['_csrf'] = bin2hex(random_bytes(30));

function e($data) {
    return htmlspecialchars($data);
}