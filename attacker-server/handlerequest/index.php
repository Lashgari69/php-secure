<?php
    header('Access-Control-Allow-Origin: *');
    header('Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept');


    $json = file_get_contents('php://input');
    $values = json_decode($json , true);

    if(isset($values['data'])) {
        $file = 'log.txt';
        $current = file_get_contents($file);
        $current .= $values['data'] . "\n";
        file_put_contents($file , $current);
    }
?>